import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('all')


#  needs to be jinja templated if can be yum sayn
def test_consul_installed(host):
    f = host.file('/usr/local/bin/consul')
    assert f.exists
    assert f.user == 'root'
    assert f.group == 'root'

def test_consul_http(host):
    cmd = host.run("curl -L http://localhost:8500/ui")
    assert '<title>Consul by HashiCorp</title>' in cmd.stdout
